use crate::ts::teamspeak::TeamSpeak;
use cursive::{
    menu::Tree,
    traits::Nameable,
    views::{Dialog, EditView, LinearLayout, Menubar, TextView},
    Cursive, CursiveExt, With,
};
use cursive_multiplex::Mux;
use std::path::PathBuf;

pub struct BaseView {
    siv: Cursive,
    toolbar: Menubar,
    mux: Mux,
    statusbar: Menubar,
}

impl BaseView {
    // Create new BaseView
    pub fn new(
        toolbar: Menubar,
        statusbar: Menubar,
        mux: Mux,
        shortcuts: Vec<(char, &str)>,
    ) -> Self {
        let mut base = BaseView {
            siv: Cursive::new(),
            toolbar,
            mux,
            statusbar,
        };
        for shortcut in shortcuts {
            match shortcut.1 {
                "QUIT" => base.siv.add_global_callback(shortcut.0, |s| s.quit()),
                _ => {}
            }
        }
        base
    }

    pub fn show(mut self) {
        let mut layout = LinearLayout::vertical();
        layout.add_child(self.toolbar);
        layout.add_child(self.mux);
        layout.add_child(self.statusbar);
        self.siv.add_fullscreen_layer(layout);
        self.siv.run();
    }
}

fn connection_dialog(siv: &mut Cursive, config_dir: PathBuf, key_file: PathBuf) {
    let server_address = EditView::new().with_name("server_address");
    let server_password = EditView::new().secret().with_name("server_password");
    let server_port = EditView::new().with_name("server_port");
    let nickname = EditView::new().with_name("nickname");
    let default_channel = EditView::new().with_name("default_channel");
    let channel_password = EditView::new().with_name("channel_password");
    let mut dialog_layout = LinearLayout::vertical();
    dialog_layout.add_child(TextView::new("Server address:"));
    dialog_layout.add_child(server_address);
    dialog_layout.add_child(TextView::new("Server password:"));
    dialog_layout.add_child(server_password);
    dialog_layout.add_child(TextView::new("Server port: (Optional, default is 9987)"));
    dialog_layout.add_child(server_port);
    dialog_layout.add_child(TextView::new(
        "Nickname: (Optional, default is TeamSpeakUser)",
    ));
    dialog_layout.add_child(nickname);
    dialog_layout.add_child(TextView::new("Default channel: (Optional)"));
    dialog_layout.add_child(default_channel);
    dialog_layout.add_child(TextView::new("Channel password:"));
    dialog_layout.add_child(channel_password);

    let dialog = Dialog::new()
        .title("New connection")
        .content(dialog_layout)
        .button("Connect", move |s| {
            //connect to server
            let s_addr = s
                .call_on_name("server_address", |view: &mut EditView| view.get_content())
                .unwrap();
            let s_passwd = s
                .call_on_name("server_password", |view: &mut EditView| view.get_content())
                .unwrap();
            let s_port = s
                .call_on_name("server_port", |view: &mut EditView| view.get_content())
                .unwrap();
            let u_nickname = s
                .call_on_name("nickname", |view: &mut EditView| view.get_content())
                .unwrap();
            let def_channel = s
                .call_on_name("default_channel", |view: &mut EditView| view.get_content())
                .unwrap();
            let channel_passwd = s
                .call_on_name("channel_password", |view: &mut EditView| view.get_content())
                .unwrap();
            let mut ts = TeamSpeak::new(
                s_addr.to_string(),
                s_passwd.to_string(),
                s_port.to_string(),
                u_nickname.to_string(),
                def_channel.to_string(),
                channel_passwd.to_string(),
                config_dir.to_owned(),
                key_file.to_owned(),
            );
            let con = ts.connect();
            s.pop_layer();
        })
        .dismiss_button("Cancel");
    siv.add_layer(dialog);
}

pub fn get_default_toolbar(config_dir: PathBuf, key_file: PathBuf) -> Menubar {
    let mut toolbar = Menubar::new();
    toolbar
        .add_subtree(
            "Connections",
            Tree::new()
                .leaf("Connect", move |s| {
                    connection_dialog(s, config_dir.clone(), key_file.clone());
                })
                .leaf("Disconnect from Current", |_| {})
                .leaf("Disconnect from All", |_| {})
                .leaf("Server List", |_| {})
                .delimiter()
                .leaf("Quit", |s| s.quit()),
        )
        .add_subtree("Stuff", Tree::new().leaf("More stuff", |_| {}))
        .add_subtree(
            "Bookmarks",
            Tree::new()
                .leaf("Add to Bookmarks", |_| {})
                .leaf("Manage Bookmarks", |_| {})
                .delimiter()
                .with(|tree| {
                    for i in 1..10 {
                        tree.add_leaf(format!("Server {}", i), |_| ())
                    }
                }),
        )
        .add_subtree(
            "Self",
            Tree::new()
                .leaf("Capture Profile", |_| {})
                .leaf("Playback Profile", |_| {})
                .leaf("Hotkey Profile", |_| {})
                .leaf("Sound Pack", |_| {})
                .delimiter()
                .leaf("Set Away", |_| {})
                .leaf("Activate Microphone", |_| {})
                .leaf("Mute Microphone", |_| {})
                .leaf("Mute Speakers/Microphone", |_| {})
                .delimiter()
                .leaf("Change Nickname", |_| {})
                .leaf("Request Talk Power", |_| {})
                .leaf("Set Phonetic Nickname", |_| {})
                .leaf("Set Avatar", |_| {})
                .delimiter()
                .leaf("Connection Info", |_| {}),
        )
        .add_subtree(
            "Permissions",
            Tree::new()
                .leaf("Server Groups", |_| {})
                .leaf("Channel Groups", |_| {})
                .delimiter()
                .leaf("Channel Groups of Clients", |_| {})
                .leaf("List All Clients", |_| {})
                .delimiter()
                .leaf("Privilege Keys", |_| {})
                .leaf("Use Privilege Key", |_| {}),
        )
        .add_subtree("Tools", Tree::new().leaf("Identities", |_| {}))
        .add_subtree("Help", Tree::new().leaf("Google it bitch", |_| {}));
    toolbar
}

pub fn get_default_statusbar() -> Menubar {
    let mut statusbar = Menubar::new();
    statusbar.add_leaf("This will eventually hold the statusbar", |_| {});
    statusbar
}
