<h2>What is ncts?</h2>

`ncts` aims to be an ncurses based TeamSpeak client written in Rust.\
It uses `tsclientlib` as a base for TeamSpeak related functionality, and `cursive` for the ncurses based TUI.

<h2>Why ncts?</h2>

Inspired by programs such as `ncspot`, `ncpamixer` and the likes, `ncts` was started. Currently, there isn't any TUI client for TeamSpeak, and one is definitely needed!\
The official TeamSpeak client is great for what it does, but it can't function in a non-GUI environment and as such is too resource intensive for some systems. `ncts` aims to solve this issue by not relying on a GUI whatsoever, but a TUI instead.\
`ncts` will allow more flexible control by the user using terminal commands etc. to help with automation and stuff, if the user so desires.\
Once complete, this will be available as an option to choose from alongside the official TeamSpeak client when using [TS3 MusicBot](https://gitlab.com/Bettehem/ts3-musicbot).

<h2>Current state</h2>

Currently `ncts` doesn't do much at all, but you can already make a test connection to a server which disconnects automatically right after.\
More coming soon.


<h3>Note</h3>
If it isn't obvious by now, This is *not* ready for users yet, and probably will not be for some time.
